import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { routes } from './app.routes';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { EmailComponent } from './email/email.component';
import { SignupComponent } from './signup/signup.component';
import { MembersComponent } from './members/members.component';
import { AuthguardService } from './authguard.service';

export const firebaseConfig = {
  apiKey: 'AIzaSyDfKkI4JLxr9-bpP1UM1hli8uMB9NLyvuc',
  authDomain: 'authentication-8f575.firebaseapp.com',
  databaseURL: 'https://authentication-8f575.firebaseio.com',
  projectId: 'authentication-8f575',
  storageBucket: 'authentication-8f575.appspot.com',
  messagingSenderId: '795449999517'

};


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    EmailComponent,
    SignupComponent,
    MembersComponent
  ],
  imports: [
    BrowserModule, FormsModule,  AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule, routes, BrowserAnimationsModule
  ],
  providers: [AuthguardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
